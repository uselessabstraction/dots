; Albert's init.el

;; Load Paths
(add-to-list 'load-path "~/.emacs.d/lisp/")
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")

;; Theme
(when (display-graphic-p)
  (load-theme 'xresources t)
  (set-frame-parameter (selected-frame) 'alpha '(90 . 90))
  (add-to-list 'default-frame-alist '(alpha . (90 . 90))))

;; Extra package sources
(require 'package)
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)

;; Includes
(require 'cc-mode)
(require 'fill-column-indicator)
(require 'org)
(require 'persistent-scratch)
(require 'tramp)
(require 'web-mode)

;; Don't litter!
(setq custom-file "~/.emacs.d/custom.el")
(setq make-backup-files nil)

;; User interface
(setq frame-title-format (concat "%b - Emacs " emacs-version))
(setq inhibit-splash-screen t)
(menu-bar-mode -1)
(tool-bar-mode -1) 
(toggle-scroll-bar -1)
(setq column-number-mode t)
(when (display-graphic-p) (set-frame-width (selected-frame) 82))
;(pixel-scroll-mode t)

;; Font
(set-frame-font "firacode 8" nil t)

;; Fill Column Indicator
(setq fci-rule-column 80)
(define-globalized-minor-mode global-fci-mode fci-mode turn-on-fci-mode)
(global-fci-mode 1)

;; Persistent Org Mode *scratch* buffer
(setq initial-major-mode 'org-mode
      persistent-scratch-save-file "~/.emacs.d/scratch"
      initial-scratch-message "\
#+TITLE: Persistent Scratch Buffer

This buffer is for random notes, shortcuts, and assorted hacks.")
(persistent-scratch-setup-default)

;; Tramp
(setq tramp-default-method "ssh")

;;
;; End of global settings ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;

;;
;; Text, Org, Markdown
;;

(add-hook 'text-mode-hook 'turn-on-auto-fill)
(add-hook 'org-mode-hook 'turn-on-auto-fill)

;;
;; Configuration - /etc, ini, yaml
;;

;;
;; Web - html, css, php, js
;;

; autocomplete sources for css and html
(setq web-mode-ac-sources-alist
  '(("css" . (ac-source-css-property))
    ("html" . (ac-source-words-in-buffer ac-source-abbrev))))

; use web-mode for PHP and assorted template files
;(add-to-list 'auto-mode-alist '("\\.twig\\'" . web-mode))
;(setq web-mode-engines-alist '(("twig" . "\\.twig\\'")))

;;
;; C / C++ / Java
;;
(setq-default c-default-style "stroustrup"
	      c-basic-offset 4)
(define-key c-mode-base-map (kbd "RET") 'newline-and-indent)

;;
;; Python
;;
(add-hook 'python-mode-hook
    (lambda ()
    (setq indent-tabs-mode t)
    (setq tab-width 4)
    (setq python-indent 4)))

;; ;; Load customizations set through UI
(load custom-file)
