-- Lua script for extra Conky functionality

do
   local fortune
end

function conky_startup()
   local file = io.popen('fortune | fmt -w 32 -t4', 'r')
   fortune = file:read('*all')
   file:close()
end

function conky_draw_pre()
end


function conky_fortune()
   return fortune
end
